package mayaclient

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/mayachain/mayanode/common"
	"gitlab.com/mayachain/mayanode/constants"
	"gitlab.com/mayachain/mayanode/x/mayachain/types"
)

// GetLastObservedInHeight returns the lastobservedin value for the chain past in
func (b *MayachainBridge) GetLastObservedInHeight(chain common.Chain) (int64, error) {
	lastblock, err := b.getLastBlock(chain)
	if err != nil {
		return 0, fmt.Errorf("failed to GetLastObservedInHeight: %w", err)
	}
	for _, item := range lastblock {
		if item.Chain == chain {
			return item.LastChainHeight, nil
		}
	}
	return 0, fmt.Errorf("fail to GetLastObservedInHeight,chain(%s)", chain)
}

// GetLastSignedOutHeight returns the lastsignedout value for mayachain
func (b *MayachainBridge) GetLastSignedOutHeight(chain common.Chain) (int64, error) {
	lastblock, err := b.getLastBlock(chain)
	if err != nil {
		return 0, fmt.Errorf("failed to GetLastSignedOutHeight: %w", err)
	}
	for _, item := range lastblock {
		if item.Chain == chain {
			return item.LastSignedHeight, nil
		}
	}
	return 0, fmt.Errorf("fail to GetLastSignedOutHeight,chain(%s)", chain)
}

// GetBlockHeight returns the current height for mayachain blocks
func (b *MayachainBridge) GetBlockHeight() (int64, error) {
	if time.Since(b.lastBlockHeightCheck) < constants.MayachainBlockTime && b.lastMayachainBlockHeight > 0 {
		return b.lastMayachainBlockHeight, nil
	}
	latestBlocks, err := b.getLastBlock(common.EmptyChain)
	if err != nil {
		return 0, fmt.Errorf("failed to GetMayachainHeight: %w", err)
	}
	b.lastBlockHeightCheck = time.Now()
	for _, item := range latestBlocks {
		b.lastMayachainBlockHeight = item.Mayachain
		return item.Mayachain, nil
	}
	return 0, fmt.Errorf("failed to GetMayachainHeight")
}

// getLastBlock calls the /lastblock/{chain} endpoint and Unmarshal's into the QueryResLastBlockHeights type
func (b *MayachainBridge) getLastBlock(chain common.Chain) ([]types.QueryResLastBlockHeights, error) {
	path := LastBlockEndpoint
	if !chain.IsEmpty() {
		path = fmt.Sprintf("%s/%s", path, chain.String())
	}
	buf, _, err := b.getWithPath(path)
	if err != nil {
		return nil, fmt.Errorf("failed to get lastblock: %w", err)
	}
	var lastBlock []types.QueryResLastBlockHeights
	if err := json.Unmarshal(buf, &lastBlock); err != nil {
		return nil, fmt.Errorf("failed to unmarshal last block: %w", err)
	}
	return lastBlock, nil
}
